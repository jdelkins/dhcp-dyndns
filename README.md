% DHCP-DYNDNS(8)

# NAME

	dhcp-dyndns

# SYNOPSIS

	dhcp-dyndns {add | delete} ip-addr hostname [mac-address]

# DESCRIPTION

_dhcp-dyndns_ is a system to update remote Windows AD DNS server from ISC dhcpd
server, using GSSAPI Kerberos authentication to facilitate the RPC call.

This package comes in 3 parts:

1. A configuration file fragment to be inserted into `dhcpd.conf` on the dhcp
   server.

2. An upstart script that launches `k5start`, to maintain a live Kerberos
   credential cache, which will permit a remote procedure call to a Windows
   Active Directory domain controller (which supposedly hosts the master DNS
   server for the domain).

3. A shell script that, using bind9's nsupdate with GSSAPI features, sends
   commands to the Windows AD server to update the DNS records.

# INSTALLATION

* You have to create a user (as configured here, named `dhcpd_proxy`) on the
  Windows AD machine, which has authority to update DNS records. Recent Windows
  Server versions include a predefined security group "DNSUpdateProxy" that
  grants the correct permissions.

* You must have a keytab file prepared, with the credentials to access the
  account configured above. This keytab file can be generated using the
  following general procedure:

		ktutil
		ktutil: addent -password -p dhcpduser@EXAMPLE.COM -k 1 -e aes256-cts-hmac-sha1-96
		Password for dhcpduser@EXAMPLE.COM: <password>
		ktutil: wkt dhcpduser.keytab
		ktutil: quit

* Link or copy the upstart configuration to `/etc/init`. This keeps the
  credential cache alive.

* Use the following in the proper place(s) in `dhcpd.conf` to ensure the script
  is properly called:

		include "/path/to/dhcpd-dyndns"

* Your installation of `isc-dhcp-server` may require you to add exceptions to
  its apparmor profile to allow the program to read the configuration fragment,
  the kerberos credential cache, and to execute the shell script. You might be
  able to put something like the following in your
  `/etc/apparmor.d/local/usr.sbin.dhcpd`:

		/var/lib/dhcp/dhcp-dyndns.cc rw,
		/usr/local/src/dhcp-dyndns/dhcp-dyndns rux,
		/usr/local/src/dhcp-dyndns/dhcpd.conf-dyndns-fragment r,

# AUTHOR

Joel D. Elkins <joel@elkins.co>

Original concept due to [Michael
Kuron](http://blog.michael.kuron-germany.de/2011/02/isc-dhcpd-dynamic-dns-updates-against-secure-microsoft-dns/).

